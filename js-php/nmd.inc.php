<?php
// Common PHP functions for working with REST API

class NetMediaData {
	public $endpoint = 'http://vps02.bhost.ru:8080/NMDBase';
	private $domainId;
	private $domainKey;
	
	function __construct($domainId, $domainKey) {
		$this->domainId = $domainId;
		$this->domainKey = $domainKey;
	}
	
	protected function requestUrl($call) {
		$url = $this->endpoint . '/domains/' . $this->domainId . $call;
		//trigger_error("Proceeding to request $url");
		return $url;
	}
	
	function sso($id, $nickname, $email, $avatar = null, $url = null) {
		$data = "$id|$nickname|$email";
		if($avatar) {
			$data = "$data|$avatar";
			if($url) {
				$data = "$data|$url";
			}
		}
	    $message = base64_encode($data);
	    $timestamp = time();
	    $hmac = hash_hmac('sha1', "$message $timestamp", $this->domainKey);
		return $message.'-'.$hmac.'-'.$timestamp;
	}
	
	function doRequest($call, $auth = null, $method = 'GET', $parameters = array()) {
		$url = $this->requestUrl($call);
		$ch = curl_init($url);
		//echo "CURL: $url / $ch<br>";
		$headers = array('Accept: application/json');
		if($parameters != null) {
			array_push($headers, 'Content-Type: application/json');
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
		}
		if($auth != null && $auth != 'API') {
			array_push($headers, "Authenticate-NMD: $auth");
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
		if($result){
			return json_decode($result);
		} else {
			return false;
		}
	}
}


?>