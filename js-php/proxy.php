<?php
// Simple proxying of AJAX requests
//
// Some browsers still don't support cross-domain AJAX

require_once 'nmd.inc.php';

$nmd = new NetMediaData(null, null);
// Some PHP setups (e.g. CGI SAPI) may not provide corrent PATH_INFO
// it is crucial that your setup will populate the variable correctly.
// Alternatively, front-end can be configured to do the proxying instead
// of this script.
$ch = curl_init($nmd->endpoint . $_SERVER['PATH_INFO']);
$headers = array('Accept: application/json');
if(isset($_SERVER['HTTP_AUTHENTICATE_NMD'])) {
	array_push($headers, "Authenticate-NMD: ".$_SERVER['HTTP_AUTHENTICATE_NMD']);
}
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
$result = curl_exec($ch);
curl_close($ch);

if($result) {
	header("Content-type: application/json");
	echo $result;
}
?>