<!DOCTYPE html>
<html>
<head>
	<title>Dynamic script - AJAX API Sample</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="nmd.js"></script>
</head><body>
	
	<p>This is an example of automatic creation of Topic on server side using PHP.</p>

	<p>Topic object in API:</p>
<pre>	
<?php
require_once 'nmd.inc.php';

define('DOMAIN_ID', '4eea030530042bf763af5c07');
define('DOMAIN_KEY', 'AtTnQV8VOWz9y7nsRfZkco2TWhUMuv');

// construct NetMediaData object using sample Domain ID and Domain Key
$nmd = new NetMediaData(DOMAIN_ID, DOMAIN_KEY);

// generate signature for the user to send it in Authenticate-NMD request header
$sso = $nmd->sso('123', 'Topic Creator', 'test@nobody.org');

// send request to create Topic
//
// in production you must store returned Topic ID locally and not make more than one
// request to the server. Authough subsequent requests for same URL will return
// the same Topic object, they will slow down your scripts.

// When forming resource URL, strip any unneccessary parameters from it,
// such as session ID, page number, etc. (if any)
$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
// Associative array is the most effortless way to represent API object in PHP
// Topic object have many attributes but the most essential and only required is url
$topic = array(
	'url' => $url
);
// send request and get response from server
// first parameter is the name of function to call. It must start from '/'.
//       NetMediaData class appends it to the URL of API and '/domains/{domainId}' prefix
// second parameter is SSO authentication string or 'API', which means that API
//		authentication will be used
// third parameter is request method, GET by default
// fourth parameter is the object to transmit, empty array by default
//
// doRequest returns Topic object from the server of false if an error occured
$topic = $nmd->doRequest('/topics', $sso, 'POST', $topic);

var_dump($topic);
?>
</pre>

<p>So what have we accomplished so far?</p>

<p>We ensured that there is a corrensponding Topic created in API for the current page
and retrieved it. Topic MUST be created in API before recording any video
responses to it (e.g. before first 'precreate' method call).</p>

<p>If you use dynamically generated pages, it is recommended to store Topic ID 
in your own database with the rest of the content to improve speed of the
'precreate' call.</p>

<p>Now, when the Topic exists on API, we may query it to display the responses.</p>
	
<pre><?php
$responses = $nmd->doRequest('/topics/'.$topic->id.'/responses');
var_dump($responses);
?></pre>

<p>Click <button onClick="respond()">Respond</button> button to load flash video
recorder in IFRAME.</p>

<script>
var endpoint = '<?php echo $nmd->endpoint; ?>';
var domainId = '<?php echo DOMAIN_ID; ?>';
var topicId = '<?php echo $topic->id; ?>';
var sso = '<?php echo $sso; ?>';

function respond() {
		var url = endpoint + '/domains/' + domainId + '/topics/' + topicId + '/responses/precreate';
		$('#recorder').attr('src', url + '?sso=' + sso);
		$('#recorder').show();
}
</script>

<iframe id="recorder" style="display:none;width:320px;height:280px"></iframe>

<p>IFRAME recorder handles entire video recording and submission. After processing
	video, REST API will raise <code><a href="../events.html#onPostResponseAction">onPostResponseAction</a></code>
	event using callback URL.</p>

<p>If you prefer to use your own scripts for recording, then instead of calling
	<code><a href="../response.hmtl#precreate">precreate</a></code> in IFRAME you may create
	a separate page that will make REST request to <code>precreate</code> to
	obtain neccessary parameters, 
	
</body></html>