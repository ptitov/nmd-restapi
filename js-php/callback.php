<?php
// This is an example of events handler
// It simply logs request and query string to the file
//
// You MUST have pecl_http installed before using this script
// To install it type: sudo pecl install pecl_http
$handler = fopen('callbacks.txt', 'a');
fputs($handler, $_SERVER['QUERY_STRING']."\n");
fputs($handler, http_get_request_body()."\n\n");
fclose($handler);
?>